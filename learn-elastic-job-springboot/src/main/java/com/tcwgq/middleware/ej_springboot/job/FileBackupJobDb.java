package com.tcwgq.middleware.ej_springboot.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.tcwgq.middleware.ej_springboot.model.FileCustom;
import com.tcwgq.middleware.ej_springboot.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 文件备份任务
 *
 * @author Administrator
 * @version 1.0
 **/
@Slf4j
@Component
public class FileBackupJobDb implements SimpleJob {
    @Autowired
    FileService fileService;

    // 任务执行代码逻辑
    @Override
    public void execute(ShardingContext shardingContext) {
        // 分片参数，（0=text,1=image,2=radio,3=video，参数就是text、image...）
        String jobParameter = shardingContext.getShardingParameter();
        // 获取未备份的文件
        // 每次任务执行要备份文件的数量
        List<FileCustom> fileCustoms = fileService.fetchUnBackupFiles(jobParameter, 1);
        log.info("作业分片：{}，time:{}，获取文件 {} 个", shardingContext.getShardingItem(), LocalDateTime.now(), 1);
        // 进行文件备份
        fileService.backupFiles(fileCustoms);
    }

}

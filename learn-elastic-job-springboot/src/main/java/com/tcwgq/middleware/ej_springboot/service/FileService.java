package com.tcwgq.middleware.ej_springboot.service;

import com.tcwgq.middleware.ej_springboot.model.FileCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 **/
@Slf4j
@Service
public class FileService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 获取某文件类型未备份的文件
     */
    public List<FileCustom> fetchUnBackupFiles(String fileType, Integer count) {
        String sql = "select * from t_file where type = ? and backedUp = 0 limit 0,?";
        List<FileCustom> files = jdbcTemplate.query(sql, new Object[]{fileType, count}, new BeanPropertyRowMapper(FileCustom.class));
        return files;
    }

    /**
     * 备份文件
     */
    public void backupFiles(List<FileCustom> files) {
        for (FileCustom fileCustom : files) {
            String sql = "update t_file set backedUp = 1 where id = ?";
            jdbcTemplate.update(sql, new Object[]{fileCustom.getId()});
        }
    }

}

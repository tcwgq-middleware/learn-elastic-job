package com.tcwgq.middleware.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author iyb-wangguangqiang 2019/7/30 17:37
 */
@Slf4j
public class MyDataFlowJob implements DataflowJob<Integer> {
    @Override
    public List<Integer> fetchData(ShardingContext shardingContext) {
        String shardingParameter = shardingContext.getShardingParameter();
        log.info("shardingParameter {}", shardingParameter);
        String jobParameter = shardingContext.getJobParameter();
        log.info("jobParameter {}", jobParameter);
        int shardingItem = shardingContext.getShardingItem();
        log.info("shardingItem {}", shardingItem);
        return Lists.newArrayList(1, 2, 3, 4);
    }

    @Override
    public void processData(ShardingContext shardingContext, List<Integer> data) {
        log.info("data {}", data);
    }
}

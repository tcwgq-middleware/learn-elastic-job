package com.tcwgq.middleware;

import com.alibaba.druid.pool.DruidDataSource;
import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.dataflow.DataflowJobConfiguration;
import com.dangdang.ddframe.job.event.JobEventConfiguration;
import com.dangdang.ddframe.job.event.rdb.JobEventRdbConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.tcwgq.middleware.job.MyDataFlowJob;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ZookeeperRegistryCenter registryCenter = new ZookeeperRegistryCenter(new ZookeeperConfiguration("localhost:2181", "elastic-job"));
        registryCenter.init();
        JobCoreConfiguration coreConfiguration = JobCoreConfiguration.newBuilder("myDataFlowJob", "0/2 * * * * ?", 3).jobParameter("aaa").shardingItemParameters("0=A,1=B,2=C").build();
        DataflowJobConfiguration dataflowJobConfiguration = new DataflowJobConfiguration(coreConfiguration, MyDataFlowJob.class.getCanonicalName(), true);
        LiteJobConfiguration build = LiteJobConfiguration.newBuilder(dataflowJobConfiguration).build();
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/elastic?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        dataSource.setUsername("root");
        dataSource.setPassword("112113");
        JobEventConfiguration jobEventConfiguration = new JobEventRdbConfiguration(dataSource);
        new JobScheduler(registryCenter, build, jobEventConfiguration).init();
    }
}
